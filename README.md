# soal-shift-sisop-modul-2-D03-2022

Kelompok D 03 \
Anggota Kelompok 
|Nama                   |     NRP|
|-----------------------|----------------------|
|Naufal Fabian Wibowo    |    05111940000223|
|Gloria Dyah Pramesti    |    5025201033|
|Selfira Ayu Sheehan     |    5025201174|

Kendala
* Sempat memiliki masalah ketika menginstall gcc
* Terdapat materi yang belum dipahami sehingga harus banyak riset

Soal 1
Penjelasan

Soal 2
Penjelasan

* 2a. mengextract zip yang diberikan ke dalam folder “/home/[user]/shift2/drakor”
fungsi unzip
![Screenshot_2022-03-27_204340](/uploads/2a6d10de0dad13b3b418414bb2fcfcde/Screenshot_2022-03-27_204340.png)

* 2b. Poster drama korea perlu dikategorikan sesuai jenisnya, maka program harus membuat folder untuk setiap jenis drama korea yang ada dalam zip. rogram harus membuatkan folder-folder yang dibutuhkan sesuai dengan isi zip.
fungsi membuat folder jenis
![Screenshot_2022-03-27_204417](/uploads/1d0be7f9fad16b62114fbaf3389b1266/Screenshot_2022-03-27_204417.png)

* 2c.program akan memindahkan poster ke folder dengan kategori yang sesuai dan di rename dengan nama
fungsi memindahkan folder dan rename
![Screenshot_2022-03-27_204537](/uploads/57f567b9af2a43513d4dcd4b5700ad8b/Screenshot_2022-03-27_204537.png)

* 2d. foto harus di pindah ke masing-masing kategori yang sesuai
fungsi memindahkan foto
![Screenshot_2022-03-27_204537](/uploads/57f567b9af2a43513d4dcd4b5700ad8b/Screenshot_2022-03-27_204537.png)

* 2e. Di setiap folder kategori drama korea buatlah sebuah file "data.txt" yang berisi nama dan tahun rilis semua drama korea dalam folder tersebut, jangan lupa untuk sorting list serial di file ini berdasarkan tahun rilis (Ascending).
fungsi data.txt 
![Screenshot_2022-03-27_204458](/uploads/e5dec636f3372c39de198701dd8235c5/Screenshot_2022-03-27_204458.png)

* 2. hasil output program
![Screenshot_2022-03-27_210251](/uploads/ae68c4330ce8fa499e126c9ad7bb4e3e/Screenshot_2022-03-27_210251.png)

Kendala
* folder yang dibuat masih tertukar, nama folder yg seharusnya genre poster menjadi judul poster.
* data.txt dan foto poster gagal dimuat karena folder tidak ada. seandainya folder ada maka muncul keduanya.
* kendala hanya di bagian pembuatan folder genre, seandainya folder berhasil dibuat seharusnya bisa berjalan dengan sesuai instruksi soal. 


Soal3
Penjelasan
* 3a. Membuat 2 directory di “/home/[USER]/modul2/” dengan nama “darat” lalu 3 detik kemudian membuat directory ke 2 dengan nama “air”

* 3b. Melakukan extract “animal.zip” di “/home/[USER]/modul2/”

* Hasil extract
![Screenshot_from_2022-03-27_13-03-49](/uploads/32a8108a35e2824a6b66c197f38e1648/Screenshot_from_2022-03-27_13-03-49.png)

* 3c. Kemudian hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya

* 3d. Menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”

* 3e. Membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt”

* Hasil didalam list.txt

![Screenshot_from_2022-03-27_13-05-58](/uploads/c722198cb31107b207f1b3f99db9fa7c/Screenshot_from_2022-03-27_13-05-58.png)
